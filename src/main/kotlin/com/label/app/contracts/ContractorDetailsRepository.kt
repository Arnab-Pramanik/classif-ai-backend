package com.label.app.contracts

import com.label.app.models.ContractorDetails
import org.springframework.data.mongodb.repository.MongoRepository

interface ContractorDetailsRepository: MongoRepository<ContractorDetails, String> {
}