package com.label.app.contracts

import com.label.app.models.Task
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository

interface TaskRepository: MongoRepository<Task, String> {
    fun findByIdAndContractorId(taskId: String, userId: String): Task?
    fun findByJobIdAndContractorId(jobId: String, userId: String, pageable: Pageable): List<Task>
    fun findByJobIdAndContractorId(jobId: String, userId: String): List<Task>
}