package com.label.app.contracts

import com.label.app.models.Company
import org.springframework.data.mongodb.repository.MongoRepository

interface CompanyRepository: MongoRepository<Company, String> {
}