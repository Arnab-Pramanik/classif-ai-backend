package com.label.app.contracts

import com.label.app.models.Job
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface JobRepository: MongoRepository<Job, String> {
    fun findByIdAndCurrentContractorId(jobId: String, userId: String): Job?
    fun findByContractorId(userId: String, pageable: Pageable): List<Job>
    fun findByIsCompletedFalse(pageable: Pageable): List<Job>
}