package com.label.app.contracts

import com.label.app.models.User
import org.springframework.data.mongodb.repository.MongoRepository

interface UserRepository: MongoRepository<User,String> {
}