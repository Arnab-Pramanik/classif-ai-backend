package com.label.app.constants

enum class Role {
    OUTSOURCER,
    CONTRACTOR
}