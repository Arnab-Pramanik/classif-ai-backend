package com.label.app.models

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document(collection = "Jobs")
data class Job(
        @Id val id: String,
        val contractorsIds: MutableList<String>,
        @Indexed var currentContractorId: String?,
        @Indexed(unique = false) val employerId: String,
        val employerName: String,
        val title: String,
        val description: String,
        val isCompleted: Boolean,
        val createdAt: Date,
        val areImagesUploadedToS3: Boolean,
        val companyId: String?
)

