package com.label.app.models

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "Tasks")
data class Task(
        @Id val id: String,
        val imageFilename: String,
        var label: String?,
        var isCompleted: Boolean,
        var contractorId: String?,
        val jobId: String
)