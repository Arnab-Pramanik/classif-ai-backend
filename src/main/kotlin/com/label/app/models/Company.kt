package com.label.app.models

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "Companies")
data class Company(
        @Id val id: String,
        val name: String
)