package com.label.app.models

import com.label.app.constants.Role
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "Users")
data class User(@Id val id: String,
                val name: String,
                val password: String,
                @Indexed(unique = true) val email: String,
                val role: List<Role>
)