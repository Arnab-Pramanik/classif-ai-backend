package com.label.app.models

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "ContractorsDetails")
data class ContractorDetails(
        @Id val id: String,
        var tasksCompletedCount: Int,
        var jobsCompletedCount: Int,
        var points: Int
)