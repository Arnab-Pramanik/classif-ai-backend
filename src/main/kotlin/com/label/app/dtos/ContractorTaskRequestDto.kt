package com.label.app.dtos

data class ContractorTaskRequestDto(
        val id: String,
        val label: String
)