package com.label.app.dtos

data class ContractorTaskResponseDto(
        val id: String,
        val imageFilename: String
)