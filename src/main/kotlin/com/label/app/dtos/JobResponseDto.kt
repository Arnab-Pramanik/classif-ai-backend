package com.label.app.dtos

data class JobResponseDto(
        val id: String,
        val employerName: String,
        val title: String,
        val description: String
)