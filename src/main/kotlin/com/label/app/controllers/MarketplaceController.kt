package com.label.app.controllers

import com.label.app.contracts.JobRepository
import com.label.app.dtos.JobResponseDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/marketplace")
class MarketplaceController @Autowired constructor(val jobRepository: JobRepository) {

    @GetMapping
    fun getAllJobs(@RequestParam("page", defaultValue = "0") page: Int): List<JobResponseDto> {
        val jobs = jobRepository.findByIsCompletedFalse(PageRequest.of(page,30,
                Sort.by(Sort.Direction.ASC,"createdAt")))
        return jobs.map {
            JobResponseDto(
                    id = it.id,
                    title = it.title,
                    description = it.description,
                    employerName = it.employerName
            )
        }
    }
}