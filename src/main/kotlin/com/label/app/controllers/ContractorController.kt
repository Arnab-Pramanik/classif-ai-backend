package com.label.app.controllers

import com.label.app.contracts.*
import com.label.app.dtos.ContractorTaskRequestDto
import com.label.app.dtos.ContractorTaskResponseDto
import com.label.app.dtos.JobResponseDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import java.security.Principal

@RestController
@RequestMapping("/api/contractor")
class ContractorController @Autowired constructor(val jobRepository: JobRepository,
                                                  val taskRepository: TaskRepository,
                                                  val companyRepository: CompanyRepository,
                                                  val userRepository: UserRepository,
                                                  val contractorDetailsRepository: ContractorDetailsRepository
) {

    @PutMapping("/my-jobs/{id}/submit-job")
    @Transactional
    fun submitJob(@PathVariable("id") jobId: String, currentUser: Principal): HttpStatus {
        val userId = currentUser.name
        val job = jobRepository.findByIdAndCurrentContractorId(jobId, userId) ?: return HttpStatus.BAD_REQUEST
        job.currentContractorId = null
        jobRepository.save(job)
        val tasks = taskRepository.findByJobIdAndContractorId(job.id, userId)
        var numTaskCompleted = 0
        tasks.forEach {
            if(!it.isCompleted) it.contractorId = null else numTaskCompleted++
        }
        taskRepository.saveAll(tasks)
        val contractorDetailsOptional = contractorDetailsRepository.findById(userId)
        if (!contractorDetailsOptional.isPresent) return HttpStatus.BAD_REQUEST
        val contractorDetails = contractorDetailsOptional.get()
        contractorDetails.jobsCompletedCount++
        contractorDetails.tasksCompletedCount += numTaskCompleted
        contractorDetails.points += numTaskCompleted * 10
        contractorDetailsRepository.save(contractorDetails)
        return HttpStatus.OK
    }

    @PutMapping("/tasks/submit-task")
    fun submitTask(@RequestBody taskDto: ContractorTaskRequestDto, currentUser: Principal): HttpStatus {
        val userId = currentUser.name
        val task = taskRepository.findByIdAndContractorId(taskDto.id, userId) ?: return HttpStatus.BAD_REQUEST
        task.label = taskDto.label
        task.isCompleted = true
        taskRepository.save(task)
        return HttpStatus.OK
    }

    @GetMapping("/my-jobs")
    fun getAllJobs(@RequestParam("page", defaultValue = "0") page: Int, currentUser: Principal): List<JobResponseDto> {
        val userId = currentUser.name
        val jobs = jobRepository.findByContractorId(userId, PageRequest.of(page,10,
                Sort.by(Sort.Direction.DESC, "createdAt")))
        return jobs.map {
            JobResponseDto(
                    id = it.id,
                    employerName = it.employerName,
                    title = it.title,
                    description = it.description
            )
        }
    }

    @GetMapping("/my-jobs/{id}/tasks")
    fun getAllTasks(@PathVariable("id") jobId: String, @RequestParam("page", defaultValue = "0") page: Int,
                    currentUser: Principal): List<ContractorTaskResponseDto> {
        val userId = currentUser.name
        val tasks = taskRepository.findByJobIdAndContractorId(jobId,userId, PageRequest.of(page, 20))
        return tasks.map {
            ContractorTaskResponseDto(
                    id = it.id,
                    imageFilename = it.imageFilename
            )
        }
    }

    @PutMapping("/start-job/{id}")
    fun startJob(@PathVariable("id") jobId: String, currentUser: Principal): HttpStatus {
        val jobOptional = jobRepository.findById(jobId)
        if(!jobOptional.isPresent) {
            return HttpStatus.BAD_REQUEST
        }
        val job = jobOptional.get()
        job.currentContractorId = currentUser.name // this is userId
        job.contractorsIds.add(currentUser.name)
        jobRepository.save(job)
        return HttpStatus.OK
    }
}